package fr.tse.poc.service;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import fr.tse.poc.dao.SessionRepository;
import fr.tse.poc.domain.Session;
import fr.tse.poc.domain.User;

import java.time.LocalDate;
import java.util.Collection;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
public class SessionServiceTests {

    @Autowired
    private UserService userService;

    @Autowired
    private SessionService sessionService;

    @Autowired 
    private SessionRepository sessionRepository;
    @Test
    public void testCreateSession(){
        User user = userService.findAllUsers().get(0);
        Session session = sessionService.createSession(user);
        Assert.assertEquals(user, session.getLoggedUser());
        String tokenSession = session.getToken();
        Assert.assertNotNull(session.getToken());
        Long sessionID = session.getId();
        
        Session newSessionForSameUser = sessionService.createSession(user);
        Assert.assertEquals(sessionID,newSessionForSameUser.getId());
        Assert.assertEquals(tokenSession, newSessionForSameUser.getToken());
        Assert.assertEquals(user,newSessionForSameUser.getLoggedUser());
        Assert.assertEquals(session, newSessionForSameUser);
    }
    
    @Test
    public void testGetUserFromToken() {
        User user = userService.findAllUsers().get(0);
        Session session = sessionService.createSession(user);
        String tokenSession = session.getToken();
        
        User userFromSessionToken  = sessionService.getLoggedUserByTokenSession(tokenSession);
        Assert.assertEquals(user, userFromSessionToken);
    }
    @Test
    public void testGetUserFromTokenInvalid() {

        User userFromInvalidToken  = sessionService.getLoggedUserByTokenSession("ran-dom-inval-idto-ken");
        Assert.assertNull(userFromInvalidToken);
    }
}

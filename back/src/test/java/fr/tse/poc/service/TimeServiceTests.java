package fr.tse.poc.service;

import fr.tse.poc.dao.ProjectRepository;
import fr.tse.poc.dao.UserRepository;
import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.Time;
import fr.tse.poc.domain.User;
import fr.tse.poc.service.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
public class TimeServiceTests {

    @Autowired
    private TimeService timeService;
    @Autowired 
    private ProjectRepository projectRepository;
    @Autowired 
    private UserRepository userRepository;

    @Test
    public void testFindAllTimes(){
        Collection<Time> times = this.timeService.findAllTimes();
        Assert.assertEquals(2, times.size());
    }

    @Test
    public void testAddTime(){
        Optional<Project> project = projectRepository.findAll().stream().filter(p -> p.getName().equals("POC")).findAny();
        Time time = new Time(0.5f, "description", LocalDate.now(), LocalDate.now());
        Assert.assertEquals(2, project.get().getTimes().size());

        time.setProject(project.get());
        User user = userRepository.findAll().stream().filter(p -> p.getFirstname().equals("Eric")).findAny().get();
        Assert.assertEquals(1, user.getTimes().size());
        time.setUser(user);
        time =this.timeService.addTime(time);
        Long timeID = time.getId();
        user = userRepository.findAll().stream().filter(p -> p.getFirstname().equals("Eric")).findAny().get();
        project = projectRepository.findAll().stream().filter(p -> p.getName().equals("POC")).findAny();
        Assert.assertEquals(3, project.get().getTimes().size());
        Assert.assertEquals(2, user.getTimes().size());
        Assert.assertTrue(user.getTimes().contains(time));
        Assert.assertTrue(project.get().getTimes().contains(time));
        
        Collection<Time> times = this.timeService.findAllTimes();
        Assert.assertEquals(3, times.size());

        this.timeService.deleteTime(time);

    }

    @Test
    public void testUpdateTime(){
        Time time = new Time(0.2f, "description", LocalDate.of(2021,02,01), LocalDate.of(2021,02,01));
        User touse = userRepository.findAll().get(0);
        Project project = projectRepository.findAll().stream().filter(p -> p.getName().equals("POC")).findAny().get();
        time.setUser(touse);
        time.setProject(project);

        time = this.timeService.addTime(time);
        Long timeID = time.getId();
        // The given time is update in JPA repository
        Time targetedTime = new Time();
        targetedTime.setDescription("description changed");
        targetedTime.setDuration(0.8f);
        targetedTime.setCreationDate(LocalDate.of(2021, 01, 01));
        Time updatedTime = this.timeService.updateTime(targetedTime,time);
        Assert.assertEquals(timeID, updatedTime.getId());
        
        updatedTime = timeService.findTime(timeID);
        
        Assert.assertEquals("description changed", updatedTime.getDescription());
        Assert.assertEquals((Float)0.8f,updatedTime.getDuration());
        Assert.assertEquals(LocalDate.of(2021, 01, 01), updatedTime.getCreationDate());


        this.timeService.deleteTime(time);
    }
    
    public void testDeleteTime() {
        Optional<Project> project = projectRepository.findAll().stream().filter(p -> p.getName().equals("POC")).findAny();
        Time time = new Time(0.5f, "description", LocalDate.now(), LocalDate.now());
        
        time.setProject(project.get());
        User user = userRepository.findAll().get(0);
    	
        time.setUser(user);
        time =this.timeService.addTime(time);
        
        
        
        Long timeID = time.getId();
        this.timeService.deleteTime(time);
        
        user = userRepository.findAll().get(0);
        project = projectRepository.findAll().stream().filter(p -> p.getName().equals("POC")).findAny();
        
        Time newTime = timeService.findTime(timeID);
       
        Assert.assertNull(newTime);
        
        Assert.assertEquals(2, project.get().getTimes().size());
        Assert.assertEquals(1, user.getTimes().size());
        Collection<Time> times = this.timeService.findAllTimes();
        Assert.assertEquals(2, times.size());

    }

}

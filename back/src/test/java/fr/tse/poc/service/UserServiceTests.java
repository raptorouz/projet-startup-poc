package fr.tse.poc.service;

import fr.tse.poc.domain.Role;
import fr.tse.poc.domain.User;
import fr.tse.poc.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
public class UserServiceTests {

    @Autowired
    private UserService userService;

    @Test
    public void testFindAllUsers(){
        Collection<User> users = this.userService.findAllUsers();

        Assert.assertEquals(6, users.size());
    }

    @Test
    public void testFindUserById(){
        User user = this.userService.findAllUsers().iterator().next();
        Assert.assertEquals(user, userService.findUser(user.getId()));
    }

    @Test
    public void testAllUsersRoles(){
        Collection<User> users = this.userService.findAllUsers();

        long admin      = users.stream().filter(user -> user.getRole() == 0).count();
        long developers = users.stream().filter(user -> user.getRole() == 1).count();
        long manager    = users.stream().filter(user -> user.getRole() == 2).count();

        Assert.assertEquals(1, admin);
        Assert.assertEquals(3, developers);
        Assert.assertEquals(2, manager);
    }

    @Test
    public void testAllManagers(){
        Collection<User> managers = this.userService.findAllManagers();

        Assert.assertEquals(2, managers.size());
    }

    @Test
    public void testCreateUser(){
        User manager = new User( "John", "Doe", "john.doe@email.com" , "password", null, Role.MANAGER);
        User dev = new User( "Doe", "John", "doe.john@email.com" , "password", manager, Role.DEVELOPER);

        manager = userService.createUser(manager);
        dev = userService.createUser(dev);

        List<User> users = this.userService.findAllUsers();
        Assert.assertEquals(8, users.size());

        // Check if the developer has the manager id that we created
        Assert.assertEquals(manager.getId(), dev.getManager().getId());

        userService.deleteUser(dev);
        userService.deleteUser(manager);
    }

    @Test
    public void testCreateUserThatAlreadyExists(){
        User manager = new User( "John", "Doe", "john.doe@email.com" , "password", null, Role.MANAGER);

        manager = userService.createUser(manager);

        List<User> users = this.userService.findAllUsers();
        Assert.assertEquals(7, users.size());

        User managerError = userService.createUser(manager);
        Assert.assertEquals(managerError, null);

        userService.deleteUser(manager);

    }

    @Test
    public void testFindManagers(){

        User manager = new User( "John", "Doe", "john.doe@email.com" , "password", null, Role.MANAGER);
        userService.createUser(manager);

        List<User> managers = this.userService.findAllManagers();

        Assert.assertEquals(3,managers.size());
        Assert.assertTrue(managers.contains(manager));

    }


    @Test 
    public void testLoginUser() {
    	User admin = userService.findAllAdmins().get(0);
    	User adminFromLogin = userService.login("admin.admin@telecom-st-etienne.fr", "password");
    	Assert.assertEquals(admin, adminFromLogin);
    	
    	User invalidPasswordAdmin = userService.login("admin.admin@telecom-st-etienne.fr", "invalidpassword");
    	Assert.assertNull(invalidPasswordAdmin);
    	
    	User invalidEmailAdmin = userService.login("invalid.admin@telecom-st-etienne.fr", "password");
    	Assert.assertNull(invalidEmailAdmin);    	
    }
}

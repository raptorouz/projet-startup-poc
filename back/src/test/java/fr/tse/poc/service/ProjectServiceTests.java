package fr.tse.poc.service;

import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.Time;
import fr.tse.poc.service.ProjectService;
import fr.tse.poc.service.TimeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
public class ProjectServiceTests {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private TimeService timeService;

    @Test
    public void testFindAllProjects(){
        Collection<Project> projects = this.projectService.findAllProjects();
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void testCreateProject(){

        Project project = new Project("Application", "TSE", LocalDate.now());

        projectService.createProject(project);
        Collection<Project> projects = this.projectService.findAllProjects();

        Assert.assertEquals(3, projects.size());

        projectService.deleteProject(project);
    }
	/*
	 * @Test public void testAddTimeToProject(){
	 * 
	 * //Project project = this.projectService.findAllProjects().iterator().next();
	 * Project project = new Project("Application N-TIERS", "TSE", LocalDate.now());
	 * project = projectService.createProject(project); String projectName =
	 * project.getName(); Long projectID = project.getId(); Assert.assertEquals(0,
	 * project.getTimes().size());
	 * 
	 * Time time = new Time(0.5f, "description", LocalDate.now(), LocalDate.now());
	 * time = this.timeService.addTime(time); Assert.assertNull(time.getProject());
	 * Long timedId = time.getId(); project =
	 * this.projectService.addTimeToProject(project, time); Long projectID2 =
	 * project.getId(); String projectName2= project.getName(); Long timeId2 = (
	 * (Time) project.getTimes().toArray()[0]).getId();
	 * 
	 * time = timeService.findTime(timeId2);
	 * Assert.assertEquals(project,time.getProject());
	 * 
	 * Assert.assertEquals(1, project.getTimes().size());
	 * Assert.assertTrue(project.getTimes().contains(time));
	 * Assert.assertEquals(projectName,projectName2);
	 * Assert.assertEquals(projectID,projectID2); Assert.assertEquals(timedId,
	 * timeId2);
	 * 
	 * 
	 * projectService.deleteProject(project); project =
	 * projectService.findProjectById(projectID2); Assert.assertNull(project); time
	 * = timeService.findTime(timeId2); Assert.assertNull(time);
	 * 
	 * }
	 */

    @Test
    public void testUpdateProject(){
        Project project = new Project("Big Data", "Google", LocalDate.of(2021,01,01));

        project = this.projectService.createProject(project);
        Long projectID = project.getId();
        Set<Time> oldTimesList = project.getTimes();
        
        
        Project newProject = new Project();
        newProject.setCompany("Modified Company");
        newProject.setName("Modified Name");
        // update project
        project = this.projectService.updateProject(newProject,project);
        
        Assert.assertEquals(projectID, project.getId());
        Assert.assertEquals(oldTimesList, project.getTimes());
        Assert.assertEquals("Modified Name", project.getName());
        Assert.assertEquals("Modified Company", project.getCompany());

        this.projectService.deleteProject(project);
    }

}

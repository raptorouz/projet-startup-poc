package fr.tse.poc.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import fr.tse.poc.dao.UserRepository;
import fr.tse.poc.domain.Role;
import fr.tse.poc.domain.User;
import fr.tse.poc.service.UserService;
import org.hamcrest.collection.IsCollectionWithSize;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class UserControllerTests {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;
    @Test
    public void testGetDevelopers() throws Exception {
        mvc.perform(get("/developers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstname", is("Cedric")))
                .andExpect(jsonPath("$[0].lastname", is("Gormond")))
                .andExpect(jsonPath("$[0].role", is(1)))
                .andExpect(jsonPath("$", new IsCollectionWithSize<User>(is(3))))
        ;
    }

    @Test
    public void testGetDevelopersWithManagerId() throws Exception {
        mvc.perform(get("/developers/?id_manager=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].role", is(1)))
                .andExpect(jsonPath("$", new IsCollectionWithSize<User>(is(3))))
        ;
    }

    @Test
    public void testGetManagers() throws Exception {
        mvc.perform(get("/managers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstname", is("John")))
                .andExpect(jsonPath("$[0].lastname", is("Doe")))
                .andExpect(jsonPath("$[0].role", is(2)))
        ;
    }

    @Test
    public void testCreateAdmin() throws Exception {
        User admin = new User( "admin", "admin", "admin@email.com" , "password", null, Role.ADMIN);

        String adminAsJSON = new JSONObject()
                .put("firstname", "admin")
                .put("lastname", "admin")
                .put("password", "admin")
                .put("email", "admin@email.com")
                .put("manager", null)
                .put("role", 0)
                .toString();

        // POST with the manager
        mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(adminAsJSON))
                .andExpect(status().is(400))
        ;

    }

    @Test
    public void testCreateUserWithFakeToken() throws Exception {
        User admin = new User( "admin", "admin", "admin@email.com" , "password", null, Role.ADMIN);

        String adminAsJSON = new JSONObject()
                .put("firstname", "admin")
                .put("lastname", "admin")
                .put("password", "admin")
                .put("email", "admin@email.com")
                .put("manager", null)
                .put("role", 0)
                .toString();

        // POST with the manager
        mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(adminAsJSON)
                .header("Authorization", "7Fdf76.3d/dsq$fdsf"))
                .andExpect(status().is(401))
        ;

    }

    public void testCreateDeveloperAndManager() throws Exception {
        User manager = new User( "manager", "manager", "manager@email.com" , "password", null, Role.MANAGER);

        String managerAsJSON = new JSONObject()
                .put("firstname", "manager")
                .put("lastname", "manager")
                .put("password", "manager")
                .put("email", "manager@email.com")
                .put("manager", null)
                .put("role", 2)
                .toString();


        // POST with the manager
        mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(managerAsJSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        ;

        Collection<User> users = userService.findAllUsers();
        Assert.assertEquals(6, users.size());
        manager = userService.findAllManagers().get(1);
        Long managerId = manager.getId();
        // POST with the developer
        User dev = new User( "developer", "developer", "developer@email.com" , "password", manager, Role.DEVELOPER);

        String devAsJSON = new JSONObject()
                .put("firstname", "developer")
                .put("lastname", "developer")
                .put("password", "developer")
                .put("email", "developer@email.com")
                .put("manager", new JSONObject().put("id", managerId))
                .put("role", 1)
                .toString();
        MvcResult result = mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(devAsJSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        ;

        users = userService.findAllUsers();
        Assert.assertEquals(7, users.size());

        Integer idInt = JsonPath.read(result.getResponse().getContentAsString(), "$.id");
        Long id = new Long(idInt);
        dev = userService.findUser(id);
        Assert.assertEquals(manager, dev.getManager());
        userService.deleteUser(dev);
        userService.deleteUser(manager);
        users = userService.findAllUsers();
        Assert.assertEquals(5, users.size());
    }

}

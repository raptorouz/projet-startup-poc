package fr.tse.poc.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class TimeControllerTests {

    @Autowired
    protected MockMvc mvc;

    @Test
    public void testGetTimes() throws Exception {

		/*
		 * mvc.perform(get("/times/project/1") .contentType(MediaType.APPLICATION_JSON))
		 * .andExpect(status().isOk())
		 * .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		 * .andExpect(jsonPath("$[0].description", is("Front End development")))
		 * .andExpect(jsonPath("$[0].duration", is(0.2)))
		 * .andExpect(jsonPath("$[1].description", is("Back End development")))
		 * .andExpect(jsonPath("$[1].duration", is(0.5))) ;
		 */
    }

}

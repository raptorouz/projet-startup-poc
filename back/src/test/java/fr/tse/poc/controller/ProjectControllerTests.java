package fr.tse.poc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.Role;
import fr.tse.poc.domain.User;
import fr.tse.poc.service.ProjectService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class ProjectControllerTests {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    private ProjectService projectService;

    @Test
    public void testGetProjects() throws Exception {

        mvc.perform(get("/projects")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].company", is("TSE")))
                .andExpect(jsonPath("$[0].name", is("Kanban")))
                .andExpect(jsonPath("$[1].company", is("TSE")))
                .andExpect(jsonPath("$[1].name", is("POC")))
        ;
    }

    @Test
    public void testUpdateProject() throws Exception {

        Project projectToPatch         = new Project("Android App", "TSE", LocalDate.now());
        Project projectWithNewerFields = new Project("iOS App", "TSE", LocalDate.now());

        ObjectMapper mapper = new ObjectMapper();
        byte[] projectWithNewerFieldsAsBytes = mapper.writeValueAsBytes(projectWithNewerFields);

        projectToPatch = projectService.createProject(projectToPatch);
        Long id = projectToPatch.getId();
        Collection<Project> projects = projectService.findAllProjects();
        projects = projectService.findAllProjects();


        for (Project currentProject: projects) {

            if(currentProject.getName().equals("Android App")){

                //byte[] projectAsBytes = mapper.writeValueAsBytes(currentProject);

                mvc.perform(patch("/projects/"+id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(projectWithNewerFieldsAsBytes))
                        .andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                ;
            }
        }

        projects = this.projectService.findAllProjects();

        boolean hasChanged = false;
        for (Project currentProject: projects) {
            if(currentProject.getName().equals("iOS App")){
                hasChanged = true;
            }
        }

        Assert.assertEquals(3, projects.size());
        Assert.assertTrue(hasChanged);

    }

    @Test
    public void testCreateProject() throws Exception {

        Project project = new Project("Java2E", "TSE", LocalDate.now());

        ObjectMapper mapper = new ObjectMapper();
        byte[] projectAsBytes = mapper.writeValueAsBytes(project);

        // POST
        mvc.perform(post("/projects")
                .contentType(MediaType.APPLICATION_JSON).content(projectAsBytes))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        ;

        Collection<Project> projects = this.projectService.findAllProjects();
        Assert.assertEquals(3, projects.size());

        Optional<Project> projectToDelete = projects.stream().filter(p -> p.getName().equals("Java2E")).findAny();
        projectService.deleteProject(projectToDelete.get());
    }
}

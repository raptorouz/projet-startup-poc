package fr.tse.poc.integration;

import fr.tse.poc.controller.ProjectControllerTests;
import fr.tse.poc.controller.TimeController;
import fr.tse.poc.controller.TimeControllerTests;
import fr.tse.poc.controller.UserControllerTests;
import fr.tse.poc.service.ProjectServiceTests;
import fr.tse.poc.service.SessionServiceTests;
import fr.tse.poc.service.TimeServiceTests;
import fr.tse.poc.service.UserServiceTests;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;


@RunWith(Suite.class)

/**
 * SuiteClasses from JUnit that run all unit tests.
 *
 */
@Suite.SuiteClasses({
        ProjectServiceTests.class,
        TimeServiceTests.class,
        UserServiceTests.class,
        ProjectControllerTests.class,
        UserControllerTests.class,
        TimeControllerTests.class,
        SessionServiceTests.class
})
public class RunAll {

    /**
     * Run all unit tests at once.
     */
    @Test
    public void testAllTests(){
        Result result = JUnitCore.runClasses(this.getClass());

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println("All Unit Tests are successful : " + result.wasSuccessful());
    }
}

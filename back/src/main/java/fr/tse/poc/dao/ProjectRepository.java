package fr.tse.poc.dao;

import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 */
public interface ProjectRepository extends JpaRepository<Project,  Long> {
    public Project findByName(String name);

}

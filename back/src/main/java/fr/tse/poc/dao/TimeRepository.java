package fr.tse.poc.dao;

import fr.tse.poc.domain.Time;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 */
public interface TimeRepository extends JpaRepository<Time, Long> {

	Collection<Time> findAllByUserIdIn(List<Long> managedUsers);

	Collection<Time> findByUserIdAndProjectId(Long id, Long projectId);

	Collection<Time> findAllByUserId(Long userId);
	
	Collection<Time> findAllByUserIdOrderByProcessDateDesc(Long userId);

	Collection<Time> findAllByUserIdInAndProjectId(List<Long> managedUsers, Long projectId);

	Collection<Time> findByUserIdAndProjectIdOrderByProcessDateDesc(Long id, Long projectId);

	Collection<Time> findAllByUserIdInOrderByProcessDateDesc(List<Long> managedUsers);

	Collection<Time> findAllByUserIdInAndProjectIdOrderByProcessDateDesc(List<Long> managedUsers, Long projectId);
}

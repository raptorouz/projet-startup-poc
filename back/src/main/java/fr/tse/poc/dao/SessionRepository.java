package fr.tse.poc.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.tse.poc.domain.Session;
import fr.tse.poc.domain.User;

public interface SessionRepository extends JpaRepository<Session, Long> {
	
	public Session findByLoggedUserId(Long LoggedUserId);
	public boolean existsByLoggedUserId(Long LoggedUserId);
	public Optional<Session> findByToken(String token);

}

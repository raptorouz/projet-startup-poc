package fr.tse.poc.dao;

import fr.tse.poc.domain.Role;
import fr.tse.poc.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

/**
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

    public List<User> findByRole(int roleOrdinal);
    

	public User findByEmail(String email);

}

package fr.tse.poc.controller;

import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.Time;
import fr.tse.poc.domain.User;
import fr.tse.poc.service.ProjectService;
import fr.tse.poc.service.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Collection;

/**
 * Controllers that use {@link ProjectService}.
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH})
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @GetMapping("/projects")
    Collection<Project> findAllProjects() {
        return this.projectService.findAllProjects();
    }

    @GetMapping("/projects/{id}")
    Project findProjectById(@PathVariable Long id) {
        return this.projectService.findProjectById(id);
    }

    @PostMapping("/projects")
    Project createProject(@RequestBody Project project) {
        return this.projectService.createProject(project);
    }

    @PatchMapping(path = "/projects/{id}")
    Project updateProjectById(@RequestBody Project project, @PathVariable Long id){
    	Project sourceProject = projectService.findProjectById(id);
        return this.projectService.updateProject(project,sourceProject);
    }

    @DeleteMapping("/projects/{id}")
    void deleteProject(@PathVariable Long id, HttpServletResponse response) {
        Project project = projectService.findProjectById(id);
        projectService.deleteProject(project);
        response.setStatus(HttpStatus.NO_CONTENT.value());

    }
}

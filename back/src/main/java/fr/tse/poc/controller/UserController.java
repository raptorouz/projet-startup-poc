package fr.tse.poc.controller;

import fr.tse.poc.domain.Role;
import fr.tse.poc.domain.Session;
import fr.tse.poc.domain.Time;
import fr.tse.poc.domain.User;
import fr.tse.poc.exceptions.InvalidLoginException;
import fr.tse.poc.service.ProjectService;
import fr.tse.poc.service.SessionService;
import fr.tse.poc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Controllers that use {@link UserService}.
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH})
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
	private SessionService sessionService;

    @GetMapping("/users")
    Collection<User> findAllUsers(@RequestHeader("Authorization") String token) {
    	User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);
    	if (loggedUser == null) {
    		throw new InvalidLoginException("Bad credentials (invalid token inserted)");
    	}
    	if (loggedUser.isAdmin()) {
    		return this.userService.findAllUsers();
    	}
    	if(loggedUser.isManager()) {
    		return this.userService.findAllDevelopersFromManagerId(loggedUser.getId());
    	}
    	return null;
    }

    @GetMapping("/users/{id}")
    User findUserById(@PathVariable Long id) {
        return this.userService.findUser(id);
    }

    @GetMapping("/admins")
    Collection<User> findAllAdmins() {
        return this.userService.findAllAdmins();
    }

    @GetMapping("/managers")
    Collection<User> findAllManagers() {
        return this.userService.findAllManagers();
    }

    @GetMapping("/developers")
    Collection<User> findAllDevelopers(@RequestParam Optional<Long> id_manager) {
        List<User> developers = new ArrayList<User>();

        // The id_manager is optional
        if (id_manager.isEmpty()){
            developers = this.userService.findAllDevelopers();
        } else {
            developers = this.userService.findAllDevelopersFromManagerId(id_manager.get());
        }

        return developers;
    }

    @PatchMapping("/users/{userId}/update/manager")
    User updateManagerOfADeveloper(@PathVariable Long userId, @RequestParam(name = "manager_id") Long managerId,@RequestHeader("Authorization") String token) {
    	User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);

    	if (loggedUser == null) {
    		throw new InvalidLoginException("Bad credentials (invalid token inserted)");
    	}

    	if (!loggedUser.isAdmin()) {
    		throw new InvalidLoginException("You're not admin so you can't modify someone's manager");
    	}

    	if(userId == managerId){
            throw new InvalidLoginException("A manager cannot have an another manager");
        }

    	User userToChange = this.userService.findUser(userId);
    	User managerToAssign = this.userService.findUser(managerId);

    	if (userToChange == null || managerToAssign == null ) {
    		throw new InvalidLoginException("Please insert a valid user ID");

    	}
    	return this.userService.updateManagerUser(userToChange, managerToAssign);
    	
    }

    @PatchMapping("/users/{userId}/update/role")
    User updateRoleOfAUser(@PathVariable Long userId, @RequestParam(name = "role_id") Long roleId, @RequestHeader("Authorization") String token) {
        User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);

        if (loggedUser == null) {
            throw new InvalidLoginException("Bad credentials (invalid token inserted)");
        }
        if (!loggedUser.isAdmin()) {
            throw new InvalidLoginException("You're not admin so you can't modify someone's role");
        }


        User userToChange = this.userService.findUser(userId);

        if (userToChange == null) {
            throw new InvalidLoginException("Please insert a valid user ID");

        }

        return this.userService.updateRoleUser(userToChange,   Role.fromOrdinal(roleId.intValue()));

    }
    
    @PostMapping("/users")
    User createUser(@Valid @RequestBody User user, @RequestHeader("Authorization") String token) {

        User userToCreate = userService.createUser(user);
        User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);

        if (loggedUser == null) {
            throw new InvalidLoginException("Bad credentials (invalid token inserted)");
        }

        if (loggedUser.isDeveloper()) {
            throw new InvalidLoginException("Insufficient permissions");
        }

        if(userToCreate == null){
            throw new InvalidLoginException("User already exists");
        }

        return userToCreate;
    }

    @PostMapping("/login")
    Session loginUser(@RequestParam String email, @RequestParam String password) {
    	User loggedUser = userService.login(email, password);
    	if (loggedUser != null) {
    		return sessionService.createSession(loggedUser);
    	}
    	else {
    		throw new InvalidLoginException("Bad credentials");
    	}
    }
}

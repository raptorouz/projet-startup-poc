package fr.tse.poc.controller;

import com.lowagie.text.DocumentException;
import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.Role;
import fr.tse.poc.domain.Session;
import fr.tse.poc.domain.Time;
import fr.tse.poc.domain.User;
import fr.tse.poc.exceptions.InvalidLoginException;
import fr.tse.poc.service.ProjectService;
import fr.tse.poc.service.SessionService;
import fr.tse.poc.service.TimeService;
import fr.tse.poc.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Controllers that use {@link TimeService}.
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH})
public class TimeController {
    @Autowired
    private TimeService timeService;
    @Autowired
    private SessionService sessionService;
    private Session session = new Session();
    @Autowired
    private ProjectService projectService;
    @Autowired
	private UserService userService;
    
    
    @GetMapping("/times/user/{userId}")
    Collection<Time> findAllTimesOfAUser(@PathVariable Long userId,@RequestHeader("Authorization") String token) {
    	User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);
    	if (loggedUser == null) {
    		throw new InvalidLoginException("Bad credentials (invalid token inserted)");
    	}
    	User userToFind = this.userService.findUser(userId);
    	if (userToFind == loggedUser) {
        	return this.timeService.findAllByUserId(userId);
    	}
    	if (userToFind == null) {
    		throw new InvalidLoginException("Bad user_id inserted");
    	}
    	if (loggedUser.isDeveloper()) {
        	throw new InvalidLoginException("Insufficient permissions, you can't see times of user " + userToFind.getFirstname() + " "+userToFind.getLastname());
    	}
    	if (loggedUser.isManager()) {
    		try {
	    		if (!userToFind.getManager().equals(loggedUser)){
	        		throw new InvalidLoginException("Insufficient permission, user_id insereted ("+userToFind.getFirstname()+" "+userToFind.getLastname()+")  is not managed by you ");
	    		}
    		}
	    	catch(NullPointerException ex) {
        		throw new InvalidLoginException("User ID provided("+userToFind.getFirstname()+" "+userToFind.getLastname()+")  is a manager or and Admin,so you can't see their times ");

	    	}
    	}
    	return this.timeService.findAllByUserId(userId);

    }
    
    @GetMapping("/times/project/{projectId}")
    Collection<Time> findProjectById(@PathVariable Long projectId,@RequestHeader("Authorization") String token) {

    	User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);
    	if (loggedUser == null) {
    		throw new InvalidLoginException("Bad credentials (invalid token or user)");
    	}
    	if(loggedUser.getRole() == Role.DEVELOPER.ordinal()) {
    		return this.timeService.findByUserIdAndProjectId(loggedUser.getId(),projectId);
    	}
    	else if(loggedUser.getRole() == Role.ADMIN.ordinal()) {
            return this.projectService.findProjectById(projectId).getTimes().stream().sorted(Collections.reverseOrder(Comparator.comparing(Time::getProcessDate))).collect(Collectors.toList());
    	}
    	else if (loggedUser.getRole() == Role.MANAGER.ordinal()) {
    		List<Long> managedUsers = this.userService.findAllDevelopersFromManagerId(loggedUser.getId()).stream()
    				.map(User::getId)
    				.collect(Collectors.toList());
    		managedUsers.add(loggedUser.getId());
    		return this.timeService.findByUserIdInAndProjectId(managedUsers,projectId);
    	}
    	return null;

    }

    @GetMapping("/times/{id}")
    Time findTimeById(@PathVariable Long id) {
        return this.timeService.findTime(id);
    }

	@GetMapping("/times/export-pdf/project/{projectId}/year/{year}/month/{month}")
	public void exportToPdfProject(HttpServletResponse response,@PathVariable Long projectId, @PathVariable int year, @PathVariable int month,@RequestHeader("Authorization") String token) throws DocumentException, IOException {
		
		Collection<Time> listTimes = null;
    	User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);
    	if (loggedUser == null) {
    		throw new InvalidLoginException("Bad credentials (invalid token or user)");
    	}
    	if(loggedUser.getRole() == Role.DEVELOPER.ordinal()) {
    		listTimes= this.timeService.findByUserIdAndProjectId(loggedUser.getId(),projectId);
    	}
    	else if(loggedUser.getRole() == Role.ADMIN.ordinal()) {
            listTimes= this.projectService.findProjectById(projectId).getTimes();
    	}
    	else if (loggedUser.getRole() == Role.MANAGER.ordinal()) {
    		List<Long> managedUsers = this.userService.findAllDevelopersFromManagerId(loggedUser.getId()).stream()
    				.map(User::getId)
    				.collect(Collectors.toList());
    		managedUsers.add(loggedUser.getId());
    		listTimes = this.timeService.findByUserIdInAndProjectId(managedUsers,projectId);
    	}
		response.setContentType("application/pdf");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=times_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);

		 listTimes = listTimes
				.stream()
				.filter(time ->
					time.getProcessDate().getMonthValue() == month && time.getProcessDate().getYear() == year)
				.collect(Collectors.toList());

		this.timeService.exportProject(response, this.projectService.findProjectById(projectId), (List<Time>) listTimes, year,month);

	}

	@GetMapping("/times/export-pdf/user/{userId}/year/{year}/month/{month}")
	public void exportToPdfUser(HttpServletResponse response, @PathVariable Long userId, @PathVariable int year, @PathVariable int month,@RequestHeader("Authorization") String token) throws DocumentException, IOException {

    	User loggedUser = this.sessionService.getLoggedUserByTokenSession(token);
    	User userToFind = this.userService.findUser(userId);
    	if (loggedUser == null) {
    		throw new InvalidLoginException("Bad credentials (invalid token or user)");
    	}
    	if(loggedUser.getId() != userId) {
    		if(loggedUser.isDeveloper()) {
        		throw new InvalidLoginException("Insufiscient permissions to export " + userToFind.getFirstname() + " " + userToFind.getLastname());
    		}
    		else if (loggedUser.isManager() && !userToFind.getManager().equals(loggedUser)) {
        		throw new InvalidLoginException("Insufiscient permissions to export " + userToFind.getFirstname() + " " + userToFind.getLastname());    			
    		}
    	}
    	
		response.setContentType("application/pdf");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=times_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);

		Collection<Time> listTimes = this.timeService.findAllByUserId(userId)
				.stream()
				.filter(time ->
						time.getProcessDate().getMonthValue() == month && time.getProcessDate().getYear() == year)
				.collect(Collectors.toList());

		this.timeService.exportUserProjects(response, this.userService.findUser(userId), (List<Time>) listTimes, year, month);

	}


    @PostMapping("/times")
    Time createTime(@RequestBody Time time) {
        return timeService.addTime(time);
    }

    @PatchMapping("/times")
    Time updateTime(@RequestBody Time time) {
    	Time sourceTime = timeService.findTime(time.getId());
    	return timeService.updateTime(time, sourceTime);
    }

    @DeleteMapping("/times/{id}")
    void deleteTime(@PathVariable Long id, HttpServletResponse response) {
    	Time timeToDelete = timeService.findTime(id);
    	timeService.deleteTime(timeToDelete);
    	response.setStatus(HttpStatus.NO_CONTENT.value());
    	
    }
}


package fr.tse.poc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.tse.poc.exceptions.InvalidLoginException;

@ControllerAdvice
public class ValidationExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", status.value());
		List<String> errorMessagesList = new ArrayList<>();
		ex.getBindingResult().getFieldErrors().stream()
		.forEach(fieldError -> errorMessagesList.add(fieldError.getDefaultMessage()));
		body.put("errors", errorMessagesList);

		return new ResponseEntity<>(body, headers, status);
	}
    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", HttpStatus.FORBIDDEN);
		body.put("error",ex.getMessage());
        return new ResponseEntity<>(body,headers,HttpStatus.BAD_REQUEST);
    }
    
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", HttpStatus.FORBIDDEN);
		body.put("error",ex.getMessage());
        return new ResponseEntity<>(body,headers,HttpStatus.BAD_REQUEST);
    }
    
	@Override
	public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", HttpStatus.BAD_REQUEST);
		body.put("error",ex.getMessage());

		return new ResponseEntity<>(body, headers, status);
	}
	
    @ExceptionHandler(InvalidLoginException.class)
    public ResponseEntity<Object> handleBadCredentials(InvalidLoginException ex) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", HttpStatus.FORBIDDEN);
		body.put("error",ex.getMessage());
		if(ex.getMessage() == "Bad credentials (invalid token or user)" || ex.getMessage()=="Bad credentials (invalid token inserted)") {
			body.put("status", HttpStatus.UNAUTHORIZED);
	        return new ResponseEntity<>(body,HttpStatus.UNAUTHORIZED);
		}
        return new ResponseEntity<>(body,HttpStatus.OK);

    }
}

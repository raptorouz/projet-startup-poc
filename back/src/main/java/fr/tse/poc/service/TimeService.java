/**
 * 
 */
package fr.tse.poc.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfPTable;
import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.Time;
import fr.tse.poc.domain.User;

import javax.servlet.http.HttpServletResponse;

/**
 *
 *
 */
public interface TimeService {
	
	public List<Time> findAllTimes();
	
	public Time findTime(Long id);

	public Time addTime(Time time);

	public void deleteTime(Time time);

	public Time updateTime(Time targetedTime, Time time);

	public Collection<Time> findByUserIdIn(List<Long> managedUsers);

	public Collection<Time> findByUserIdAndProjectId(Long id, Long projectId);

	public Collection<Time> findAllByUserId(Long userId);

	public void writeTableHeaderUserProject(PdfPTable table);

	public float writeTableDataUserProject(PdfPTable table, Collection<Time> listTimes);

	public void exportProject(HttpServletResponse response,Project project, List<Time> listTimes, int year, int month) throws DocumentException, IOException;

	public void writeTableHeaderUserProjects(PdfPTable table);

	public float writeTableDataUserProjects(PdfPTable table, Collection<Time> listTimes);

	public void exportUserProjects(HttpServletResponse response,User user, List<Time> listTimes, int year, int month) throws DocumentException, IOException;

	public Collection<Time> findByUserIdInAndProjectId(List<Long> managedUsers, Long projectId);
}

package fr.tse.poc.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.poc.dao.SessionRepository;
import fr.tse.poc.domain.Session;
import fr.tse.poc.domain.User;
import fr.tse.poc.service.SessionService;

@Service
public class SessionServiceImpl implements SessionService {
	
	@Autowired 
	SessionRepository sessionRepository;
	@Override
	public String generateToken() {
        String token = UUID.randomUUID().toString();
        return token;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean userAlreadyHaveSession(User user) {
		return sessionRepository.existsByLoggedUserId(user.getId());

	}

	@Override
	@Transactional
	public Session createSession(User user) {
		if (userAlreadyHaveSession(user)) {
			return sessionRepository.findByLoggedUserId(user.getId());
		}
		else {
			Session newSession = new Session();
			newSession.setToken(generateToken());
			newSession.setLoggedUser(user);
			return sessionRepository.save(newSession);
			
		}
	}

	@Override
	@Transactional(readOnly = true)
	public  User getLoggedUserByTokenSession(String token) {
		Optional<Session> session = sessionRepository.findByToken(token);
		return session.isPresent() ? session.get().getLoggedUser() : null;
	}
	
	
	public boolean isTokenValidForRequest(String token) {
		return false;
		
	}


}

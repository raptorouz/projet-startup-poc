/**
 * 
 */
package fr.tse.poc.service;

import java.time.LocalDate;
import java.util.List;

import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.Time;

/**
 *
 *
 */
public interface ProjectService {
	
	public List<Project> findAllProjects();
	
	public Project findProjectById(Long id);

	public Project createProject(Project project);

	public void deleteProject(Project project);

	public Project addTimeToProject(Project project, Time time);

	public Project updateProject(Project project, Project sourceProject);

}

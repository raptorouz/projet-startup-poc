package fr.tse.poc.service.impl;

import java.awt.*;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import fr.tse.poc.domain.Project;
import fr.tse.poc.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.poc.dao.ProjectRepository;
import fr.tse.poc.dao.TimeRepository;
import fr.tse.poc.dao.UserRepository;
import fr.tse.poc.domain.Time;
import fr.tse.poc.service.TimeService;
import java.text.DateFormatSymbols;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

/**
 *
 *
 */
@Service
public class TimeServiceImpl implements TimeService {
	
	@Autowired
	private TimeRepository timeRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProjectRepository projectRepository;
	

	@Override
	@Transactional(readOnly = true)
	public List<Time> findAllTimes() {
		return this.timeRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Time findTime(Long id) {
		return this.timeRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Time addTime(Time time){
		if(time.getUser() != null) {
			time.getUser().addTime(time);
		}
		if(time.getProject() != null) {
			time.getProject().addTime(time);
		}
		Time timeToReturn = this.timeRepository.save(time);
		return timeToReturn;
	}

	@Override
	@Transactional
	public void deleteTime(Time time) {
		time = this.timeRepository.save(time);
		this.timeRepository.delete(time);
	}


	@Override
	@Transactional
	public Time updateTime(Time targetedTime, Time time) {
		// We get the time slot back with the correct ID
		if(targetedTime.getDuration() != null) {
			time.setDuration(targetedTime.getDuration());
		}
		if(targetedTime.getDescription() != null) {
			time.setDescription(targetedTime.getDescription());
		}
		if(targetedTime.getCreationDate() != null) {
			time.setCreationDate(targetedTime.getCreationDate());
		}
		if(targetedTime.getProcessDate() != null) {
			time.setProcessDate(targetedTime.getProcessDate());

		}

		// TODO changer le User/Projet ?
		//targetedTime.setUser(time.getUser());
		//targetedTime.setProject(time.getProject());

		return this.timeRepository.save(time);
	}

	@Override
	public Collection<Time> findByUserIdIn(List<Long> managedUsers) {
		// TODO Auto-generated method stub
		return this.timeRepository.findAllByUserIdInOrderByProcessDateDesc(managedUsers);
	}

	@Override
	public Collection<Time> findByUserIdAndProjectId(Long id, Long projectId) {
		// TODO Auto-generated method stub
		return this.timeRepository.findByUserIdAndProjectIdOrderByProcessDateDesc(id,projectId);
	}

	@Override
	public Collection<Time> findAllByUserId(Long userId) {
		// TODO Auto-generated method stub
		return this.timeRepository.findAllByUserIdOrderByProcessDateDesc(userId);
	}

	public void writeTableHeaderUserProject(PdfPTable table) {
		
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.BLUE);
		cell.setPadding(5);

		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(Color.WHITE);
		
		cell.setPhrase(new Phrase("User", font));
		table.addCell(cell);
		cell.setPhrase(new Phrase("Time ID", font));
		table.addCell(cell);


		

		cell.setPhrase(new Phrase("Duration", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Description", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Process date", font));
		table.addCell(cell);

		table.completeRow();
	}



	public float writeTableDataUserProject(PdfPTable table, Collection<Time> listTimes) {
		float sum=0;
		for (Time time : listTimes) {
			table.addCell(time.getUser().getFirstname() + " " +time.getUser().getLastname());
			table.addCell(time.getId().toString());
			table.addCell(time.getDuration().toString());
			table.addCell(time.getDescription());
			table.addCell(time.getProcessDate().toString());
			sum = sum + time.getDuration();
			table.completeRow();
		}
		return sum;
	}

	public void exportProject(HttpServletResponse response, Project project, List<Time> listTimes, int year, int month) throws DocumentException, IOException {
		Document document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, response.getOutputStream());

		document.open();
		Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font.setSize(18);
		font.setColor(Color.BLUE);

		Paragraph p1 = new Paragraph("List of times for " + project.getName() + " project", font);
		p1.setAlignment(Paragraph.ALIGN_CENTER);

		document.add(p1);

		Font font2 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font2.setSize(14);
		font2.setColor(Color.RED);

		Paragraph p2 = new Paragraph(new DateFormatSymbols(Locale.ENGLISH).getMonths()[month-1] +" "+ String.valueOf(year), font2);
		p2.setAlignment(Paragraph.ALIGN_CENTER);

		document.add(p2);

		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100f);
		table.setWidths(new float[] {2f,1f, 1f, 3f, 1f});
		table.setSpacingBefore(10);

		writeTableHeaderUserProject(table);
		float sum = writeTableDataUserProject(table, listTimes);

		document.add(table);
		if (sum != 0) {
			Paragraph p3 = new Paragraph("Total time units : " + sum);
			p3.setAlignment(Paragraph.ALIGN_RIGHT);
			document.add(p3);
		}
		document.close();

	}

	public void writeTableHeaderUserProjects(PdfPTable table) {
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.BLUE);
		cell.setPadding(5);

		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(Color.WHITE);

		cell.setPhrase(new Phrase("Project ID", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Project name", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Time ID", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Duration", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Description", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Process date", font));
		table.addCell(cell);

		table.completeRow();
	}



	public float writeTableDataUserProjects(PdfPTable table, Collection<Time> listTimes) {
		float sum = 0;
		for (Time time : listTimes) {
			table.addCell(time.getProject().getId().toString());
			table.addCell(time.getProject().getName());
			table.addCell(time.getId().toString());
			table.addCell(time.getDuration().toString());
			table.addCell(time.getDescription());
			table.addCell(time.getProcessDate().toString());
			sum = sum + time.getDuration();
			table.completeRow();
		}
		return sum;
	}

	public void exportUserProjects(HttpServletResponse response, User user, List<Time> listTimes, int year, int month) throws DocumentException, IOException {
		Document document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, response.getOutputStream());

		document.open();
		Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font.setSize(18);
		font.setColor(Color.BLUE);

		Paragraph p = new Paragraph("List of times for all projects by " + user.getFirstname() +  " " + user.getLastname(), font);
		p.setAlignment(Paragraph.ALIGN_CENTER);

		document.add(p);

		Font font2 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font2.setSize(14);
		font2.setColor(Color.RED);

		Paragraph p2 = new Paragraph(new DateFormatSymbols(Locale.ENGLISH).getMonths()[month-1] +" "+ String.valueOf(year), font2);
		p2.setAlignment(Paragraph.ALIGN_CENTER);

		document.add(p2);

		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100f);
		table.setWidths(new float[] {1.5f, 2f, 1f, 1.5f, 2.5f, 1.5f});
		table.setSpacingBefore(10);

		writeTableHeaderUserProjects(table);
		float sum = writeTableDataUserProjects(table, listTimes);

		document.add(table);
		if (sum != 0) {
			Paragraph p3 = new Paragraph("Total time units : " + sum);
			p3.setAlignment(Paragraph.ALIGN_RIGHT);
			document.add(p3);
		}

		document.close();

	}

	@Override
	public Collection<Time> findByUserIdInAndProjectId(List<Long> managedUsers, Long projectId) {
		// TODO Auto-generated method stub
		return this.timeRepository.findAllByUserIdInAndProjectIdOrderByProcessDateDesc(managedUsers,projectId);

	}

}

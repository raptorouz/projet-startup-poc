package fr.tse.poc.service;

import java.util.UUID;

import fr.tse.poc.domain.Session;
import fr.tse.poc.domain.User;

public interface SessionService {
	public String generateToken();
	public boolean userAlreadyHaveSession(User user);
	public Session createSession(User user);
	public User getLoggedUserByTokenSession(String token);
	public boolean isTokenValidForRequest(String token);
}

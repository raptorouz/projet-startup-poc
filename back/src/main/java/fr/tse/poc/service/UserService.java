/**
 * 
 */
package fr.tse.poc.service;

import java.util.List;

import fr.tse.poc.domain.Role;
import fr.tse.poc.domain.User;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 *
 */
public interface UserService {
	
	public List<User> findAllUsers();
	
	public User findUser(Long id);
	
	public User createUser(User user);

	public void deleteUser(User user);

	public List<User> findAllManagers();

	public List<User> findAllDevelopers();
	
	public User login(String email, String password);

	public List<User> findAllDevelopersFromManagerId(Long idManager);

	public List<User> findAllAdmins();

	public User updateRoleUser(User user, Role role);

	public User updateManagerUser(User user, User manager);

}

package fr.tse.poc.service.impl;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import fr.tse.poc.domain.Time;
import fr.tse.poc.service.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.poc.dao.ProjectRepository;
import fr.tse.poc.dao.TimeRepository;
import fr.tse.poc.domain.Project;
import fr.tse.poc.service.ProjectService;

/**
 *
 *
 */
@Service
public class ProjectServiceImpl implements ProjectService {
	
	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private TimeService timeService;
	@Autowired
	private TimeRepository timeRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllProjects() {
		return this.projectRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Project findProjectById(Long id) {
		return this.projectRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Project createProject(Project project){
		return this.projectRepository.save(project);
	}

	@Override
	@Transactional
	public void deleteProject(Project project) {

		// Remark : cascade={CascadeType.ALL}, orphanRemoval=true, fetch = FetchType.EAGER) should be in charge of removing all times
		project = projectRepository.save(project);

		if(!(project.getTimes() ==  null)){

			// Delete all time in a project
			for (Time time: project.getTimes()) {
				timeService.deleteTime(time);
			}
			project.getTimes().clear();
		}

		this.projectRepository.delete(project);
	}

	/**
	 * Add a given {@link Time} slot to a given {@link Project}.
	 *
	 * @param project
	 * @param timeToAdd
	 *
	 * @return
	 */
	@Override
	@Transactional
	public Project addTimeToProject(Project project, Time timeToAdd) {
		timeToAdd.setProject(project);
		timeToAdd = timeRepository.save(timeToAdd);
		if (project.getTimes() == null){
			project.setTimes(new HashSet<Time>());
			project.getTimes().add(timeToAdd);
		} else {
			project.getTimes().add(timeToAdd);
		}

		return projectRepository.save(project);
	}


	@Override
	@Transactional
	public Project updateProject(Project targetedProject, Project sourceProject) {
		if(targetedProject.getCompany() != null) {
			sourceProject.setCompany(targetedProject.getCompany());
		}
		if(targetedProject.getName() != null) {
			sourceProject.setName(targetedProject.getName());
		}
		return projectRepository.save(sourceProject);

	}

}

package fr.tse.poc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.tse.poc.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.tse.poc.dao.UserRepository;
import fr.tse.poc.domain.User;
import fr.tse.poc.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 *
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	/**
	 * Find all users (any role) in the repository.
	 *
	 * @return List of {@link User}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> findAllUsers() {
		return this.userRepository.findAll();
	}

	/**
	 * Find a user by a given id.
	 *
	 * @param id (long)
	 *
	 * @return {@link User} or null
	 */
	@Override
	@Transactional(readOnly = true)
	public User findUser(Long id) {
		return this.userRepository.findById(id).orElse(null);
	}

	/**
	 * Create a {@link User} of any role in the repository. If the
	 *
	 * @param user to save
	 *
	 * @return user saved or null if it already exist
	 */
	@Override
	@Transactional
	public User createUser(User user){

		User userCreated = null;

		Optional<User> userFound = userRepository
				.findAll()
				.stream()
				.filter(u -> u.getEmail().equals(user.getEmail()))
				.findAny();

		if (userFound.isEmpty()) {
			//userCreated = userRepository.save(user);
			user.setPassword(
					new BCryptPasswordEncoder().encode(user.getPassword()) // Password encryption by Spring Security
			);
			userCreated = userRepository.save(user);
		}

		return userCreated;
	}

	/**
	 * Delete any user.
	 *
	 * The deletion of managers will transfer the ownership of their users. In this way, we're not able to delete a
	 * manager if it is the only one remaining.
	 *
	 * @param user to delete
	 */
	@Override
	@Transactional
	public void deleteUser(User user) {
		user = this.userRepository.save(user);

		if(user.getRole()==Role.MANAGER.ordinal()) {
			User finalUser = user;

			if(findAllManagers().size() > 1) {
				User newManager = findAllManagers().get(0) != user ? findAllManagers().get(0) : findAllManagers().get(1);

				findAllDevelopers()
						.stream()
						.filter(dev -> dev.getManager().equals(finalUser))
						.forEach(u -> updateManagerUser(u,newManager));
			} else {
				return;
			}
		}


		this.userRepository.delete(user);
	}

	/**
	 * Find all managers (role 1) in the repository.
	 *
	 * @return List of {@link User}
	 */
	@Override
	@Transactional
	public List<User> findAllManagers() {
		List<User> managers = this.userRepository.findByRole(Role.MANAGER.ordinal());
		return managers;

	}

	/**
	 * Find all developers (role 2) in the repository.
	 *
	 * @return List of {@link User}
	 */
	@Override
	@Transactional
	public List<User> findAllDevelopers() {
		List<User> developers = this.userRepository.findByRole(Role.DEVELOPER.ordinal());
		return developers;

	}

	@Override
	public List<User> findAllDevelopersFromManagerId(Long idManager) {
		User manager = this.userRepository.findById(idManager).orElse(null);

		List<User> developers = findAllDevelopers()
				.stream()
				.filter(dev -> dev.getManager().getId().equals(idManager))
				.collect(Collectors.toList());

		return developers;
	}

	/**
	 * Find all admins (role 0) in the repository.
	 *
	 * @return List of {@link User}
	 */
	@Override
	@Transactional
	public List<User> findAllAdmins() {
		List<User> admins = this.userRepository.findByRole(Role.ADMIN.ordinal());
		return admins;

	}

	@Override
	@Transactional
	public User updateRoleUser(User user, Role role) {

		// We get the user slot back with the correct ID
		user = this.userRepository.save(user);

		if(role.ordinal() == user.getRole()){
			return user;
		}

		if(user.getRole()==Role.MANAGER.ordinal()){

			user.setManager(null);

			User finalUser = user;

			if(findAllManagers().size() > 1) {
				User newManager = findAllManagers().get(0) != user ? findAllManagers().get(0) : findAllManagers().get(1);

				findAllDevelopers()
						.stream()
						.filter(dev -> dev.getManager().equals(finalUser))
						.forEach(u -> updateManagerUser(u,newManager));

				user.setRole(role.ordinal());

				if(role.ordinal() == Role.DEVELOPER.ordinal()){
					user.setManager(newManager);
				}

				user = this.userRepository.save(user);
			}
		}

		else{
			if(user.getRole()!=role.ordinal()){

				user.setRole(role.ordinal());
				this.userRepository.save(user);
			}
		}

		return user;
	}

	@Override
	@Transactional
	public User updateManagerUser(User user, User manager){

		user.setManager(manager);

		return this.userRepository.save(user);

	}
	
	public User login(String email, String password) {
		User userToReturn = null;
		User foundUser = userRepository.findByEmail(email);
		if (foundUser != null) {
			userToReturn = new BCryptPasswordEncoder().matches(password, foundUser.getPassword()) ? foundUser : null;
		}
		return userToReturn;
	}
		
}

package fr.tse.poc.exceptions;



/**
 * This exception is thrown when the SuperHero can't be found in the application if searching by ID.
 *
 */
public class InvalidLoginException extends RuntimeException {

    public InvalidLoginException(final String message) {
        super(message);
    }

}

package fr.tse.poc.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Entity
@Data

public class Session {
    private @Id @GeneratedValue Long id;
    
    @OneToOne
    @JsonIgnoreProperties({"password","times"})
    private User loggedUser;
    
    private String token;
    
}

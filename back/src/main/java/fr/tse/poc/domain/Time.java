package fr.tse.poc.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

/**
 * POJO of {@link Time} class, which represents a time slot.
 *
 * @author cedric
 */
@Data
@Entity
public class Time {
    private @Id
    @GeneratedValue
    Long id;

    @DecimalMin(value="0", message="Duration must be greater than 0f")
    @DecimalMax(value="1", message="Duration must be lower than 1f")
    private Float duration;

    private String description;

    private LocalDate creationDate;

    private LocalDate processDate;

    
	@Valid
	@NotNull(message="L'utilisateur iinséré ne doit pas etre null")
    @ManyToOne(fetch=FetchType.EAGER)
    @JsonIgnoreProperties("times")
    private User user;
    
	@Valid
	@NotNull(message="Le projet inséré ne doit pas etre null")
    @JsonIgnoreProperties("times")
    @ManyToOne(fetch=FetchType.EAGER)
    private Project project;

    public Time() {
    	this.creationDate = LocalDate.now();

    }

    public Time(Float duration, String description, LocalDate creationDate, LocalDate processDate) {
        this.duration     = duration;
        this.description  = description;
        this.creationDate = creationDate;
        this.processDate  = processDate;
    }


}

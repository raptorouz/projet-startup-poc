package fr.tse.poc.domain;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;


/**
 * POJO of {@link User} class, which represents an admin, a developer or a manager.
 */
@Data
@Entity
public class User {
    private @Id @GeneratedValue Long id;

    private String firstname;

    private String lastname;

    // TODO @Email validation ?
    private String email;

    private String password;

    @ManyToOne
    @JsonIgnoreProperties("times")
    private User manager;

    private int role;


    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy="user",fetch = FetchType.EAGER)
    @JsonIgnoreProperties("user")
    private Set<Time> times;
    
    public void addTime(Time timeToAdd) {
    	times.add(timeToAdd);
    }
    
    public User() {
        this.times     = new HashSet<>();
    }

    public User(String firstname, String lastname, String email, String password, User manager, Role role) {
        this.firstname = firstname;
        this.lastname  = lastname;
        this.email     = email;
        this.password  = password;
        this.manager   = manager;
        this.role      = role.ordinal();
    }

    public boolean isManager() {
    	return this.getRole() == Role.MANAGER.ordinal();
    }

    @JsonIgnore
    public boolean isAdmin() {
    	return this.getRole() == Role.ADMIN.ordinal();
    }

    @JsonIgnore
    public boolean isDeveloper() {
    	return this.getRole() == Role.DEVELOPER.ordinal();
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
}

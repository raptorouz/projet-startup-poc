package fr.tse.poc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Data
@Entity
/**
 * POJO of {@link Project} class, which represents a company's project.
 */
public class Project {
	
	@Id @GeneratedValue
    private Long id;

    private String name;

    private String company;

    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate creationDate;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    //@JsonIgnoreProperties({"user", "project"})
    @JsonIgnoreProperties({"project"})
    @OneToMany(mappedBy="project",fetch = FetchType.EAGER)
    private Set<Time> times = new HashSet<Time>();
    
    public void addTime(Time timeToAdd) {
        times.add(timeToAdd);
    }

    public Project(String name, String company, LocalDate creationDate) {
        this.name = name;
        this.company = company;
        this.creationDate = creationDate;
    }

    public Project() {
        this.times= new HashSet<Time>();
        this.creationDate = LocalDate.now();
    }
}

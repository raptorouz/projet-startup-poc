package fr.tse.poc.domain;


/**
 * Defines 3 roles : Admin, Developer, Manager
 */

public enum Role {
    ADMIN, DEVELOPER, MANAGER;

    private static Role[] allValues = values();

    public static Role fromOrdinal(int n) {return allValues[n];}
}

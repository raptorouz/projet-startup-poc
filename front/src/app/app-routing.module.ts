import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
    },
    {
        path: 'users',
        loadChildren: () => import('./pages/users/users.module').then(m => m.UsersPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'user-detail',
        loadChildren: () => import('./pages/user-detail/user-detail.module').then(m => m.UserDetailPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'projects',
        loadChildren: () => import('./pages/projects/projects.module').then(m => m.ProjectsPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'calendar',
        loadChildren: () => import('./pages/calendar/calendar.module').then(m => m.CalendarPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'times',
        loadChildren: () => import('./pages/times/times.module').then(m => m.TimesPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'login',
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Project} from '../interface/project';
import {Time} from '../interface/time';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            //Authorization: 'my-auth-token'
        })
    };

    constructor(
        public httpApi: HttpClient
    ) {
    }

    public getProjectList(): Observable<Project[]> {
        return this.httpApi.get('http://localhost:8080/projects').pipe(
            map((response: any) => response)
        );
    }

    public getProjectById(id: number): Observable<Project> {
        return this.httpApi.get(`http://localhost:8080/projects/${id}`).pipe(
            map((response: any) => response)
        );
    }

    public getTimesOfProjectById(id: number): Observable<Time[]> {
        return this.httpApi.get(`http://localhost:8080/times/project/${id}`, {headers: {'Authorization': localStorage.getItem('token')}}).pipe(
            map((response: any) => response)
        );
    }

    public postAddProject(project: Project): Observable<Project> {

        return this.httpApi.post<Project>('http://localhost:8080/projects', project)
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

    public editProject(project: Project, id: number): Observable<Project> {
        return this.httpApi.patch<Project>(`http://localhost:8080/projects/${id}`, project)
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

    public deleteProject(id: number): Observable<Project> {
        return this.httpApi.delete<Project>(`http://localhost:8080/projects/${id}`)
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

}

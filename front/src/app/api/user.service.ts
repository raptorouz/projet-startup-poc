import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {User} from '../interface/user';
import {Time} from '../interface/time';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(
        public httpApi: HttpClient,
        public router: Router
    ) {
    }

    isAdmin() {
        return (JSON.parse(localStorage.getItem('loggedUser'))?.role) == 0;
    }

    isManager(): boolean {
        return (JSON.parse(localStorage.getItem('loggedUser'))?.role) == 2;
    }

    isDeveloper(): boolean {
        return (JSON.parse(localStorage.getItem('loggedUser'))?.role) == 1;
    }

    login(email: string, password: string): Observable<boolean> {
        return this.httpApi.post(`http://localhost:8080/login?email=${email}&password=${password}`, {}).pipe(
            map((response: any) => {
                    if (!!response?.token) {
                        localStorage.setItem('loggedUser', JSON.stringify(response.loggedUser));
                        localStorage.setItem('token', response.token);
                    }
                    return !!response?.token;
                },
                catchError(() => of(false))
            ));
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('loggedUser');
        window.location.replace('/login');
    }

    getUserList(): Observable<User[]> {
        return this.httpApi.get('http://localhost:8080/users', {headers: {'Authorization': localStorage.getItem('token')}}).pipe(
            map((response: any) => response)
        );
    }

    getDeveloperList(): Observable<User[]> {
        return this.httpApi.get('http://localhost:8080/developers').pipe(
            map((response: any) => response)
        );
    }

    getManagerList(): Observable<User[]> {
        return this.httpApi.get('http://localhost:8080/managers').pipe(
            map((response: any) => response)
        );
    }

    getUserDetail(id: number): Observable<User> {
        return this.httpApi.get(`http://localhost:8080/users/${id}`).pipe(
            map((response: any) => response)
        );
    }

    public postAddUser(user: User): Observable<User> {
        return this.httpApi.post<User>('http://localhost:8080/users', user, {headers: {'Authorization': localStorage.getItem('token')}})
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

    exportTimes(userId: number, year: number, month: number): any {
        return this.httpApi.get(`http://localhost:8080/times/export-pdf/user/${userId}/year/${year}/month/${month}`,
            {responseType: 'blob', headers: {'Authorization': localStorage.getItem('token')}}).pipe(
            map((response: any) => response),
            catchError(async (err) => console.log(err))
        );

    }

    exportTimesOfProject(projectId: number, year: number, month: number): any {
        return this.httpApi.get(`http://localhost:8080/times/export-pdf/project/${projectId}/year/${year}/month/${month}`, {
            responseType: 'blob',
            headers: {'Authorization': localStorage.getItem('token')}
        }).pipe(
            map((response: any) => response),
            catchError(async (err) => console.log(err))
        );

    }

    public editTime(time: Time, id: number): Observable<Time> {
        return this.httpApi.patch<Time>(`http://localhost:8080/times`, time)
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

    public changeManager(userId: number, managerId: number): Observable<any> {

        return this.httpApi.patch<User>(`http://localhost:8080/users/${userId}/update/manager?manager_id=${managerId}`,
            {}, {headers: {'Authorization': localStorage.getItem('token')}})
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

    public changeRole(userId: number, roleId: number): Observable<any> {

        return this.httpApi.patch<User>(`http://localhost:8080/users/${userId}/update/role?role_id=${roleId}`,
            {}, {headers: {'Authorization': localStorage.getItem('token')}})
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

}

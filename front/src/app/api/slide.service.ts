import {Injectable} from '@angular/core';
import {Platform} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class SlideService {

    splitPaneState: boolean = true;

    constructor(public platform: Platform) {
        if (this.platform.width() > 768) {
            this.splitPaneState = true;
        } else {
            this.splitPaneState = false;
        }
    }

    setSplitPane(state: boolean) {
        if (this.platform.width() > 768) {
            this.splitPaneState = state;
        } else {
            this.splitPaneState = false;
        }
    }

    getSplitPane() {
        return this.splitPaneState;
    }

}

import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Time} from '../interface/time';

@Injectable({
    providedIn: 'root'
})
export class TimeService {

    constructor(
        public httpApi: HttpClient
    ) {
    }

    getTimeList() {
        return this.httpApi.get('http://localhost:8080/times').pipe(
            map((response: any) => response)
        );
    }

    public postAddTime(time: Time): Observable<Time> {
        console.log(time);
        return this.httpApi.post<Time>('http://localhost:8080/times', time)
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

    public editTime(time: Time, id: number): Observable<Time> {
        console.log('Edit :', time);
        return this.httpApi.patch<Time>(`http://localhost:8080/times`, time)
            .pipe(
                map((response: any) => response),
                catchError(async (err) => {
                    console.log(err);
                })
            );
    }

    public deleteTime(id: number): Observable<Time> {
        return this.httpApi.delete<Time>(`http://localhost:8080/times/${id}`)
            .pipe(
                map((response: any) => response),
                catchError(async (err) => console.log(err))
            );
    }

    public getTimesOfAUser(id: number): Observable<Time[]> {
        return this.httpApi.get(`http://localhost:8080/times/user/${id}`, {headers: {'Authorization': localStorage.getItem('token')}}).pipe(
            map((response: any) => response),
            catchError(async (err) => console.log(err))
        );
    }

    isEditable(processDateStr: string): boolean {

        const processMonth = new Date(processDateStr).getMonth();
        const processYear = new Date(processDateStr).getFullYear();

        const currentDateMonth = new Date().getMonth();
        const currentDateYear = new Date().getFullYear();

        return ((processMonth >= currentDateMonth) && (processYear >= currentDateYear));
    }
}

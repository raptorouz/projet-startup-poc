import {Time} from './time';

export interface Project {
    id: number;
    name: string;
    company: string;
    creationDate: string;
    times: Time[];
}

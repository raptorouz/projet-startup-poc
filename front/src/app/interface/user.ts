import {Time} from './time';

export interface User {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    manager: User;
    role: number;
    times: Time[];
}

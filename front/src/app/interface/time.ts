import {User} from './user';

export interface Time {
    id: number;
    description: string;
    creationDate: string;
    processDate: string;
    user: User;
}

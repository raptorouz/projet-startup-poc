import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {UserDetailPageRoutingModule} from './user-detail-routing.module';

import {UserDetailPage} from './user-detail.page';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {TimeModalComponent} from '../../components/time-modal/time-modal.component';
import {TimeListComponent} from '../../components/time-list/time-list.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        UserDetailPageRoutingModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
    ],
    declarations: [UserDetailPage, TimeListComponent, TimeModalComponent],
    entryComponents: [TimeModalComponent],
})
export class UserDetailPageModule {
}

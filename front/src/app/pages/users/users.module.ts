import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {UsersPageRoutingModule} from './users-routing.module';

import {UsersPage} from './users.page';
import {UserListComponent} from '../../components/user-list/user-list.component';
import {CalendarModule} from 'ion2-calendar';
import {UserModalComponent} from '../../components/user-modal/user-modal.component';
import {SwitchUserModalComponent} from 'src/app/components/switch-user-modal/switch-user-modal.component';
import {ChangeRoleModalComponent} from '../../components/change-role-modal/change-role-modal.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        UsersPageRoutingModule,
        CalendarModule
    ],
    declarations: [UsersPage, UserListComponent, UserModalComponent, SwitchUserModalComponent, ChangeRoleModalComponent],
    entryComponents: [UserModalComponent, SwitchUserModalComponent, ChangeRoleModalComponent]
})
export class UsersPageModule {
}

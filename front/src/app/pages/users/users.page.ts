import {Component, OnInit} from '@angular/core';
import {UserService} from '../../api/user.service';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {ModalController} from '@ionic/angular';
import {User} from '../../interface/user';
import {UserModalComponent} from '../../components/user-modal/user-modal.component';

@Component({
    selector: 'app-users',
    templateUrl: './users.page.html',
    styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

    subject: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

    userList$: Observable<User[]> = this.subject.asObservable();

    isDeveloper: boolean;

    constructor(
        public router: Router,
        public userService: UserService,
        private modalController: ModalController
    ) {
    }


addUserFromCard(){
  this.addUserModal();
}

    
    async selectedUser(id: number) {
        sessionStorage.setItem('userId', String(id));
        await this.router.navigateByUrl('user-detail');
    }

    ngOnInit(): void {
        this.userService.getUserList().subscribe(users => this.subject.next(users));
        this.isDeveloper = this.userService.isDeveloper();
    }

    async addUserModal() {
        const modal = await this.modalController.create({
            component: UserModalComponent,
            componentProps: {
                isUserEditable: false,
            }
        });

        modal.onDidDismiss()
            .then(data => {
                    if (data.data) {
                        this.userService.getUserList().subscribe(
                            users => this.subject.next(users)
                        );

                    }
                }
            );
        await modal.present();
    }

    refresh(user: User) {
        this.userService.getUserList().subscribe(
            users => this.subject.next(users)
        );
    }

}

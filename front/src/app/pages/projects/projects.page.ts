import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {UserService} from '../../api/user.service';
import {ProjectService} from '../../api/project.service';
import {ModalController} from '@ionic/angular';
import {formatDate} from '@angular/common';
import {ProjectModalComponent} from '../../components/project-modal/project-modal.component';
import {take} from 'rxjs/operators';
import {Project} from 'src/app/interface/project';

@Component({
    selector: 'app-projects',
    templateUrl: './projects.page.html',
    styleUrls: ['./projects.page.scss'],
})
export class ProjectsPage implements OnInit {

    subject: BehaviorSubject<Project[]> = new BehaviorSubject<Project[]>([]);

    projectList$: Observable<Project[]> = this.subject.asObservable();
    isDeveloper: boolean;

    constructor(
        public router: Router,
        public projectService: ProjectService,
        public userService: UserService,
        private modalController: ModalController,
    ) {
    }

    async selectedProject(id: number) {
        sessionStorage.setItem('projectId', String(id));

        await this.router.navigateByUrl('calendar');
    }

    refresh(project: Project) {
        this.projectService.getProjectList().subscribe(projects => this.subject.next(projects));
    }

    ngOnInit(): void {
        this.projectService.getProjectList().subscribe(data => this.subject.next(data));
        this.isDeveloper = this.userService.isDeveloper();
    }

    async addProjectModal() {
        const modal = await this.modalController.create({
            component: ProjectModalComponent,
            componentProps: {
                isProjectEditable: false,
                creationDate: formatDate(new Date(), 'yyyy-MM-dd', 'en')
            }
        });

        modal.onDidDismiss()
            .then(data => {
                    if (data.data) {
                        this.subject
                            .pipe(take(1))
                            .subscribe((projects: Project[]) => {
                                this.subject.next([...projects, data.data]);
                            });

                    }
                }
            );
        await modal.present();
    }

}

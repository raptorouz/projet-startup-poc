import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ProjectsPageRoutingModule} from './projects-routing.module';

import {ProjectsPage} from './projects.page';
import {ProjectListComponent} from '../../components/project-list/project-list.component';
import {ProjectModalComponent} from '../../components/project-modal/project-modal.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        ProjectsPageRoutingModule
    ],
    declarations: [ProjectsPage, ProjectListComponent, ProjectModalComponent],
    entryComponents: [ProjectModalComponent]
})
export class ProjectsPageModule {
}

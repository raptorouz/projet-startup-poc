import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {CalendarEvent, CalendarEventTitleFormatter, CalendarView, collapseAnimation} from 'angular-calendar';
import {DOCUMENT, formatDate} from '@angular/common';
import {BehaviorSubject, Observable} from 'rxjs';


import {isSameDay, isSameMonth,} from 'date-fns';
import {ProjectService} from '../../api/project.service';
import {map} from 'rxjs/operators';
import {Time} from '../../interface/time';
import {ModalController} from '@ionic/angular';
import {TimeModalComponent} from '../../components/time-modal/time-modal.component';
import {TimeService} from '../../api/time.service';
import {CustomEventTitleFormatter} from 'src/app/components/custom-event-title-formatter.provider';
import {UserService} from 'src/app/api/user.service';

const COLORS: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3',
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF',
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA',
    },
};


@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.page.html',
    styleUrls: ['./calendar.page.scss'],
    providers: [
        {
            provide: CalendarEventTitleFormatter,
            useClass: CustomEventTitleFormatter,
        },
    ],
    encapsulation: ViewEncapsulation.None,
    animations: [collapseAnimation]
})
export class CalendarPage implements OnInit, OnDestroy {

    subject: BehaviorSubject<Time[]> = new BehaviorSubject<Time[]>([]);

    timeList$: Observable<Time[]> = this.subject.asObservable();

    /*
     * Calendars fields
     */
    view: CalendarView = CalendarView.Month;

    viewDate: Date = new Date();

    clickedDate: Date;

    clickedColumn: number;

    activeDayIsOpen: boolean = false;

    // Async events of the calendar
    events$: Observable<CalendarEvent<{ time: Time }>[]>;


    /**
     * Fetch every times of the current project and convert them into an Observation of CalendarEvent
     */
    fetchEvents(): void {

        const projectId = sessionStorage.getItem('projectId');

        this.events$ = this.timeList$
            .pipe(
                map((times) => {
                    return times.map((time: Time) => {
                        return {
                            title: time.description,
                            start: new Date(time.processDate),
                            color: COLORS.blue,
                            allDay: true,
                            meta: {
                                time,
                            }
                        };

                    });
                })
            );
    }

    /**
     *Fonction called when the user clicks on a day
     *
     * @param date
     * @param events
     */
    dayClicked({
                   date,
                   events,
               }: {
        date: Date;
        events: CalendarEvent<{ time: Time }>[];
    }): void {

        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                console.log('No event');
                this.activeDayIsOpen = false;
            } else {
                console.log('One or more event(s)');
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }

        }
    }

    eventClicked({event}: { event: CalendarEvent }): void {
        const time: Time = event.meta.time;
        console.log(time);
        if (this.timeService.isEditable(time.processDate)) {
            this.editTime(time);
        }
    }


    async editTime(currentTime) {
        const modal = await this.modalController.create({
            component: TimeModalComponent,
            componentProps: {
                currentDescription: currentTime.description,
                currentDuration: currentTime.duration,
                creationDate: formatDate(new Date(currentTime.creationDate), 'yyyy-MM-dd', 'en'),
                processDate: formatDate(new Date(currentTime.processDate), 'yyyy-MM-dd', 'en'),
                selectedTimeId: currentTime.id,
                isTimeEditable: true
            }
        });
        modal.onDidDismiss()
            .then((data) => {
                if (data.data) {
                    const newData: Time = data.data;
                    this.refresh(newData);
                    // this.projectList[index]=data.data;
                    // this.projectList=[...this.projectList]
                } // Here's your selected user!
            });
        return await modal.present();
    }

    private readonly darkThemeClass = 'dark-theme';

    constructor(@Inject(DOCUMENT) private document,
                public projectService: ProjectService,
                public timeService: TimeService,
                private modalController: ModalController,
                public userService: UserService
    ) {

    }

    generatePDF() {
        const projectId = sessionStorage.getItem('projectId');
        this.userService.exportTimesOfProject(parseInt(projectId, 10), this.viewDate.getFullYear(), this.viewDate.getMonth() + 1).subscribe((data) => {
            const blob = new Blob([data], {type: 'application/pdf'});

            var downloadURL = window.URL.createObjectURL(blob);
            var link = document.createElement('a');
            link.href = downloadURL;
            link.download = 'Compte rendu du projet pour la période ' + (this.viewDate.getMonth() + 1) + '/' + this.viewDate.getFullYear();
            link.click();
        });
    }

    ngOnInit(): void {
        // Id of the previous project
        const projectId = sessionStorage.getItem('projectId');

        // Observable project
        this.projectService.getTimesOfProjectById(parseInt(projectId, 10)).subscribe(times => this.subject.next(times));

        this.fetchEvents();

        // Dark mode
        this.document.body.classList.add(this.darkThemeClass);
    }

    ngOnDestroy(): void {
        this.events$.subscribe();
    }

    refresh(time: Time) {
        this.projectService.getTimesOfProjectById(parseInt(sessionStorage.getItem('projectId'), 10)).subscribe(times => this.subject.next(times));
    }

    async addTimeModal() {
        const modal = await this.modalController.create({
            component: TimeModalComponent,
            componentProps: {
                isTimeEditable: false,
                creationDate: formatDate(new Date(), 'yyyy-MM-dd', 'en'),
            }
        });
        modal.onDidDismiss()
            .then(data => {
                    if (data.data) {
                        this.projectService.getTimesOfProjectById(parseInt(sessionStorage.getItem('projectId'), 10)).subscribe(times => this.subject.next(times));

                    }
                }
            );
        return await modal.present();
    }
}

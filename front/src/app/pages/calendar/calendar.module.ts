import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

import {CalendarPageRoutingModule} from './calendar-routing.module';

import {CalendarPage} from './calendar.page';

import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {TimeListComponent} from '../../components/time-list/time-list.component';
import {TimeModalComponent} from '../../components/time-modal/time-modal.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        CalendarPageRoutingModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),

    ],
    declarations: [CalendarPage, TimeListComponent, TimeModalComponent],
    entryComponents: [TimeModalComponent],
})
export class CalendarPageModule {
}

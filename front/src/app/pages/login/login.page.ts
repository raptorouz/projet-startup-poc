import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertController, MenuController, ToastController} from '@ionic/angular';
import {UserService} from 'src/app/api/user.service';
import {Subscription} from 'rxjs';
import {SlideService} from '../../api/slide.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

    email: string;
    password: string;
    login$: Subscription;

    constructor(
        public router: Router,
        public userService: UserService,
        public alertCtrl: AlertController,
        private menu: MenuController,
        private slideService: SlideService,
        private toastController: ToastController,
        private MenuController: MenuController
    ) {
        this.menu.enable(false, 'custom');
    }

    ionViewWillEnter() {
        // Disable the split plane in this page
        //this.slideService.setSplitPane(false);
    }

    ionViewWillLeave() {
        // Enable it again when leaving the page
        //this.slideService.setSplitPane(true);
    }

    ngOnDestroy(): void {
        if (this.login$) {
            this.login$.unsubscribe();
        }
    }

    ngOnInit() {
    }

    /**
     * Display a Toast notification to the user with a custom message
     */
    async displayToastNotification(customMessage: string) {
        const toast = await this.toastController.create({
            color: 'danger',
            position: 'top',
            duration: 2000,
            message: customMessage,
        });

        await toast.present();
    }

    /**
     * Login function that is called by login button
     */
    login() {
        this.login$ = this.userService.login(this.email, this.password).subscribe(
            isLoggued => {
                if (isLoggued) {
                    window.location.replace('/times');
                } else {
                    this.displayToastNotification('Wrong email or password');
                }

            }
        );

    }
}

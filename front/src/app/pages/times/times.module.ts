import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {IonicModule} from '@ionic/angular';

import {TimesPage} from './times.page';
import {TimeListComponent} from 'src/app/components/time-list/time-list.component';
import {TimesPageRoutingModule} from './times-routing.module';
import {TimeModalComponent} from '../../components/time-modal/time-modal.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        TimesPageRoutingModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
    ],
    declarations: [TimesPage, TimeListComponent, TimeModalComponent],
    entryComponents: [TimeModalComponent],
})
export class TimesPageModule {
}

import {Component, Input, OnDestroy, OnInit} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {SlideService} from './api/slide.service';
import {UserService} from './api/user.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    public selectedIndex = 0;

    @Input()
    public appPagesProjects: any;

    loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

    public appPages = [
        {
            title: 'Times',
            url: '/times',
            icon: 'calendar',
            permissions: true
        },
        {
            title: 'My team',
            url: '/users',
            icon: 'people',
            permissions: JSON.parse(localStorage.getItem('loggedUser'))?.role != 1
        },
        {
            title: 'Projects',
            url: '/projects',
            icon: 'book',
            permissions: true
        }

    ];

    /*
    public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
    */

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private slideService: SlideService,
        private userService: UserService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    ionViewWillEnter() {
        // Enable the split plane on every page
        //  this.slideService.setSplitPane(true);
    }

    ngOnInit() {
        const path = window.location.pathname.split('folder/')[1];
        if (path !== undefined) {
            this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
        }
    }

    logout() {
        this.userService.logout();
    }

    ngOnDestroy(): void {
        //this.slideService.setSplitPane(true);
    }
}

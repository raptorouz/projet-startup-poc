import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AlertController, ModalController} from '@ionic/angular';

import {Project} from '../../interface/project';
import {formatDate} from '@angular/common';
import {ProjectModalComponent} from '../project-modal/project-modal.component';
import {Observable} from 'rxjs';
import {ProjectService} from '../../api/project.service';

@Component({
    selector: 'app-project-list',
    templateUrl: './project-list.component.html',
    styleUrls: ['./project-list.component.scss'],
})
export class ProjectListComponent implements OnInit {

    @Input()
    projectList: Observable<Project[]>;

    @Output()
    selectedProject = new EventEmitter<number>();

    @Output()
    refresh = new EventEmitter<Project>();

    @Input()
    isDeveloper: boolean;

    constructor(
        private modalController: ModalController,
        public alertController: AlertController,
        private projectService: ProjectService) {
    }

    /**
     *
     * @param currentProject
     */
    async editProject(currentProject) {
        const modal = await this.modalController.create({
            component: ProjectModalComponent,
            componentProps: {
                currentDescription: currentProject.name,
                currentCompany: currentProject.company,
                creationDate: formatDate(new Date(currentProject.creationDate), 'yyyy-MM-dd', 'en'),
                selectedProjectId: currentProject.id,
                isProjectEditable: true
            }
        });
        modal.onDidDismiss()
            .then((data) => {
                if (data.data) {
                    const newData: Project = data.data;
                    this.refresh.emit(newData);
                    //this.projectList[index]=data.data;
                    //this.projectList=[...this.projectList]
                } // Here's your selected user!
            });
        return await modal.present();
    }

    /**
     *
     * @param project
     */
    async deleteProject(project: Project) {
        let messageAlert: string = 'Delete ' + project.name;
        const alert = await this.alertController.create({
            header: 'Confirm',
            message: messageAlert,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.projectService.deleteProject(project.id).subscribe();
                        console.log('Confirm Okay');
                    }
                }
            ]
        });

        await alert.present();
    }

    /**
     *
     * @param event
     * @param id
     */
    emitProjectId(event: Event, id: number) {
        event.stopPropagation();
        this.selectedProject.emit(id);
    }

    trackByFn(index: number, project: any): number {
        return project.id;
    }


    ngOnInit() {
    }

}

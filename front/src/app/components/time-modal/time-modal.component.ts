import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalController, ToastController} from '@ionic/angular';
import {TimeService} from '../../api/time.service';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'app-time-modal',
    templateUrl: './time-modal.component.html',
    styleUrls: ['./time-modal.component.scss'],
})
export class TimeModalComponent implements OnInit {

    // Date pipe change date format
    datePipe = new DatePipe('en-US');

    time: any;

    timeForm = new FormGroup({
        descriptionInputControl: new FormControl('', [Validators.required, Validators.maxLength(200), this.noWhitespaceValidator]),
        durationInputControl: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(1)]),
        processDateInputControl: new FormControl('', [Validators.required]),
    });

    @Input()
    currentDescription: string;

    @Input()
    selectedTimeId: number;

    @Input()
    currentDuration: number;

    @Input()
    creationDate: Date;

    @Input()
    processDate: Date;

    @Input()
    isTimeEditable: boolean;


    constructor(
        private modalController: ModalController,
        private timeService: TimeService,
        private toastController: ToastController) {
    }

    ngOnInit() {
    }

    /**
     * Display a Toast notification to the user with a custom message
     */
    async displayToastNotification(customMessage: string) {
        const toast = await this.toastController.create({
            color: 'primary',
            position: 'top',
            duration: 2000,
            message: customMessage,
        });

        await toast.present();
    }

    /**
     * Cancel and go back to previous page
     */
    dismissModal() {
        this.modalController.dismiss();
    }

    /**
     *
     */
    addTime(projectFormGroup: FormGroup) {

        // Reformat date
        const processDateWithWrongDateFormat = new Date(this.processDate.toString());
        const processDateFormatted = this.datePipe.transform(processDateWithWrongDateFormat, 'yyyy-MM-dd');

        this.time = {
            id: null,
            description: this.currentDescription,
            creationDate: this.creationDate.toString(),
            duration: this.currentDuration.toString(),
            processDate: processDateFormatted,
            user: {
                id: JSON.parse(localStorage.getItem('loggedUser')).id
            },
            project: {
                id: sessionStorage.getItem('projectId')
            }
        };

        this.timeService.postAddTime(this.time).subscribe(newTime => {
            this.displayToastNotification('Project added successfully');
            this.modalController.dismiss(newTime);
        });


    }

    editTime() {

        const processDateWithWrongDateFormat = new Date(this.processDate.toString());
        const processDateFormatted = this.datePipe.transform(processDateWithWrongDateFormat, 'yyyy-MM-dd');

        this.time = {
            id: this.selectedTimeId,
            description: this.currentDescription,
            creationDate: this.creationDate.toString(),
            processDate: processDateFormatted,
            duration: this.currentDuration.toString(),
            user: null,
            project: null
        };

        this.timeService.editTime(this.time, this.selectedTimeId).subscribe(newTime => {
            this.displayToastNotification('Project edited successfully');
            this.modalController.dismiss(newTime);
        });


    }

    /**
     * Checks white space in form control
     *
     * @param control
     */
    public noWhitespaceValidator(control: FormControl) {
        const isWhitespace = (control.value || '').trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : {'whitespace': true};
    }

    /**
     * Checks
     *
     * @param control
     */
    public minDate(control: FormControl) {
        const date = control.value;
        const isValid = (control.value > this.processDate.toString());
        return isValid ? null : {'date': true};
    }

}

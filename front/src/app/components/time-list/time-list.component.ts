import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Time} from 'src/app/interface/time';
import {formatDate} from '@angular/common';
import {AlertController, ModalController} from '@ionic/angular';
import {TimeModalComponent} from '../time-modal/time-modal.component';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-time-list',
    templateUrl: './time-list.component.html',
    styleUrls: ['./time-list.component.scss'],
})
export class TimeListComponent implements OnInit {

    @Input()
    timeList: Observable<Time[]>;

    @Output()
    selectedTime = new EventEmitter<number>();

    @Output()
    refresh = new EventEmitter<Time>();

    constructor(private modalController: ModalController,
                private alertController: AlertController) {
    }

    ngOnInit() {
    }

    /**
     *
     */
    async editTime(currentTime) {
        const modal = await this.modalController.create({
            component: TimeModalComponent,
            componentProps: {
                currentDescription: currentTime.description,
                currentDuration: currentTime.duration,
                creationDate: formatDate(new Date(currentTime.creationDate), 'yyyy-MM-dd', 'en'),
                processDate: formatDate(new Date(currentTime.processDate), 'yyyy-MM-dd', 'en'),
                selectedTimeId: currentTime.id,
                isTimeEditable: true
            }
        });
        modal.onDidDismiss()
            .then((data) => {
                if (data.data) {
                    const newData: Time = data.data;
                    this.refresh.emit(newData);
                    // this.projectList[index]=data.data;
                    // this.projectList=[...this.projectList]
                } // Here's your selected user!
            });
        return await modal.present();
    }

    /**
     *
     * @param project
     */
    async deleteTime(time: Time) {
        let messageAlert: string = 'Delete ' + time.description + ' will be deleted';
        const alert = await this.alertController.create({
            header: 'Confirm',
            message: messageAlert,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                }, {
                    text: 'Delete',
                    handler: () => {
                        console.log('TODO :delete Okay');
                    }
                }
            ]
        });

        await alert.present();
    }

    trackByFn(index: number, time: any): number {
        return time.id;
    }

    isEditable(processDateStr: string): boolean {

        const processMonth = new Date(processDateStr).getMonth();
        const processYear = new Date(processDateStr).getFullYear();

        const currentDateMonth = new Date().getMonth();
        const currentDateYear = new Date().getFullYear();

        return ((processMonth >= currentDateMonth) && (processYear >= currentDateYear));
    }
}

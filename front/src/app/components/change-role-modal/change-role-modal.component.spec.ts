import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IonicModule} from '@ionic/angular';

import {ChangeRoleModalComponent} from './change-role-modal.component';

describe('ChangeRoleModalComponent', () => {
    let component: ChangeRoleModalComponent;
    let fixture: ComponentFixture<ChangeRoleModalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ChangeRoleModalComponent],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(ChangeRoleModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

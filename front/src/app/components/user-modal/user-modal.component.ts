import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalController, ToastController} from '@ionic/angular';
import {UserService} from '../../api/user.service';

@Component({
    selector: 'app-user-modal',
    templateUrl: './user-modal.component.html',
    styleUrls: ['./user-modal.component.scss'],
})
export class UserModalComponent implements OnInit {

    user: any;

    userForm = new FormGroup({
        firstnameInputControl: new FormControl('', [Validators.required, this.noWhitespaceValidator]),
        lastnameInputControl: new FormControl('', [Validators.required, this.noWhitespaceValidator]),
        passwordInputControl: new FormControl('', [Validators.required, this.noWhitespaceValidator, Validators.minLength(5)]),
        emailInputControl: new FormControl('', [Validators.required, Validators.email]),
    });

    @Input()
    currentFirstname: string;

    @Input()
    selectedUserId: number;

    @Input()
    currentLastname: string;

    @Input()
    currentEmail: string;

    /*
    @Input()
    currentRole: number;
    */

    @Input()
    currentPassword: string;

    @Input()
    isUserEditable: boolean;

    constructor(
        private modalController: ModalController,
        private userService: UserService,
        private toastController: ToastController) {
    }

    ngOnInit() {
    }

    /**
     * Display a Toast notification to the user with a custom message
     */
    async displayToastNotification(customMessage: string, customColor: string = 'primary') {
        const toast = await this.toastController.create({
            color: customColor,
            position: 'top',
            duration: 2000,
            message: customMessage,
        });

        await toast.present();
    }

    /**
     * Cancel and go back to previous page
     */
    dismissModal() {
        this.modalController.dismiss();
    }

    /**
     *
     */
    addUser() {

        this.user = {
            id: null,
            firstname: this.currentFirstname,
            lastname: this.currentLastname,
            email: this.currentEmail,
            password: this.currentPassword,
            times: null,
            role: 1,
            manager: {
                id: JSON.parse(localStorage.getItem('loggedUser')).id
            }
        };


        this.userService.postAddUser(this.user).subscribe(newUser => {
            if (newUser.id === undefined) {
                this.displayToastNotification('Email already exists', 'danger');
            } else {
                this.displayToastNotification('User added successfully');
                this.modalController.dismiss(newUser);
            }

        });


    }

    editUser() {
        this.user = {
            id: null,
            firstname: this.currentFirstname,
            lastname: this.currentLastname,
            email: this.currentEmail,
            password: this.currentPassword,
            times: null,
            role: 1,
            manager: null,
        };

        // this.userService.editProject(this.user,this.selectedProjectId).subscribe(newProject => this.modalController.dismiss(newProject));

        this.displayToastNotification('Project edited successfully');
    }

    /**
     *
     * @param control
     */
    public noWhitespaceValidator(control: FormControl) {
        const isWhitespace = (control.value || '').trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : {'whitespace': true};
    }
}

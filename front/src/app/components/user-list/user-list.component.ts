import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Project} from '../../interface/project';
import {Observable} from 'rxjs';
import {User} from 'src/app/interface/user';
import {ModalController} from '@ionic/angular';
import {SwitchUserModalComponent} from '../switch-user-modal/switch-user-modal.component';
import {ChangeRoleModalComponent} from '../change-role-modal/change-role-modal.component';
import { UserService } from 'src/app/api/user.service';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {

    @Input()
    userList: Observable<User[]>;

    @Output()
    selectedUser = new EventEmitter<number>();

    @Output()
    refresh = new EventEmitter<Project>();
    @Output()
    addUserFromCardEvent = new EventEmitter<any>();

    loggedUserIsAdmin: boolean;


    isNotDeveloper = !this.userService.isDeveloper();
    constructor(private modalController: ModalController,public userService: UserService) {
    }

    ngOnInit() {
        this.loggedUserIsAdmin = this.userService.isAdmin();
    }

    trackByFn(index: number, user: any): number {
        return user.id;
    }

addUserFromCard(){
  this.addUserFromCardEvent.emit();
}

    async swapManagers(user: User) {
        const modal = await this.modalController.create({
            component: SwitchUserModalComponent,
            componentProps: {
                currentFirstname: user.firstname,
                currentLastname: user.lastname,
                selectedUserId: user.id
            }
        });
        modal.onDidDismiss()
            .then((data) => {
                if (data.data) {
                    const newData: any = data.data;
                    this.refresh.emit(newData);
                    // this.projectList[index]=data.data;
                    // this.projectList=[...this.projectList]
                } // Here's your selected user!
            });
        return await modal.present();
    }

    async changeRole(user: User) {
        const modal = await this.modalController.create({
            component: ChangeRoleModalComponent,
            componentProps: {
                currentFirstname: user.firstname,
                currentLastname: user.lastname,
                selectedUserId: user.id
            }
        });
        modal.onDidDismiss()
            .then((data) => {
                if (data.data) {
                    const newData: any = data.data;
                    this.refresh.emit(newData);
                    // this.projectList[index]=data.data;
                    // this.projectList=[...this.projectList]
                } // Here's your selected user!
            });
        return await modal.present();
    }

}

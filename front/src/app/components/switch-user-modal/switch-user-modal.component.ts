import {Component, Input, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ModalController, ToastController} from '@ionic/angular';
import {UserService} from '../../api/user.service';
import {User} from '../../interface/user';

@Component({
    selector: 'app-switch-user-modal',
    templateUrl: './switch-user-modal.component.html',
    styleUrls: ['./switch-user-modal.component.scss'],
})
export class SwitchUserModalComponent implements OnInit {

    managers: any [];

    user: any;

    futureManager: any;

    managerSelectInputControl = new FormControl('', [Validators.required]);

    @Input()
    currentManager: any;

    @Input()
    currentFirstname: string;

    @Input()
    selectedUserId: number;

    @Input()
    currentLastname: string;

    constructor(
        private modalController: ModalController,
        private userService: UserService,
        private toastController: ToastController) {
    }

    ngOnInit() {
        this.userService.getManagerList().subscribe(managers => this.managers = managers);
    }

    /**
     * Display a Toast notification to the user with a custom message
     */
    async displayToastNotification(customMessage: string, customColor: string = 'primary') {
        const toast = await this.toastController.create({
            color: customColor,
            position: 'top',
            duration: 2000,
            message: customMessage,
        });

        await toast.present();
    }

    /**
     * Cancel and go back to previous page
     */
    dismissModal() {
        this.modalController.dismiss();
    }

    /**
     *
     */
    changeManagerUser() {

        this.userService.changeManager(this.selectedUserId, this.futureManager.id).subscribe((user: User) => this.modalController.dismiss(user));
        this.displayToastNotification('Manager successfully changed');
    }

    compareFn(e1: User, e2: User): boolean {
        return e1 && e2 ? e1.id === e2.id : e1 === e2;
    }

    compareWith(o1: User, o2: User) {
        return o1 && o2 ? o1.id === o2.id : o1 === o2;
    }
}

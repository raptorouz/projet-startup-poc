import {Component, Input, OnInit} from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProjectService} from '../../api/project.service';
import {Project} from '../../interface/project';

@Component({
    selector: 'app-project-modal',
    templateUrl: './project-modal.component.html',
    styleUrls: ['./project-modal.component.scss'],
})
export class ProjectModalComponent implements OnInit {

    project: Project;

    projectForm = new FormGroup({
        descriptionInputControl: new FormControl('', [Validators.required, this.noWhitespaceValidator]),
        companyInputControl: new FormControl('', [Validators.required, this.noWhitespaceValidator]),
    });

    @Input()
    currentDescription: string;

    @Input()
    selectedProjectId: number;

    @Input()
    currentCompany: string;

    @Input()
    creationDate: Date;

    @Input()
    isProjectEditable: boolean;

    constructor(
        private modalController: ModalController,
        private projectService: ProjectService,
        private toastController: ToastController) {
    }

    ngOnInit() {
    }

    /**
     * Display a Toast notification to the user with a custom message
     */
    async displayToastNotification(customMessage: string) {
        const toast = await this.toastController.create({
            color: 'primary',
            position: 'top',
            duration: 2000,
            message: customMessage,
        });

        await toast.present();
    }

    /**
     * Cancel and go back to previous page
     */
    dismissModal() {
        this.modalController.dismiss();
    }

    /**
     *
     */
    addProject(projectFormGroup: FormGroup) {
        this.project = {
            id: null,
            name: this.currentDescription,
            company: this.currentCompany,
            creationDate: this.creationDate.toString(),
            times: null,
        };

        this.projectService.postAddProject(this.project).subscribe(newProject => this.modalController.dismiss(newProject));

        this.displayToastNotification('Project added successfully');
    }

    editProject() {
        this.project = {
            id: null,
            name: this.currentDescription,
            company: this.currentCompany,
            creationDate: null,
            times: null,
        };

        this.projectService.editProject(this.project, this.selectedProjectId).subscribe(newProject => this.modalController.dismiss(newProject));

        this.displayToastNotification('Project edited successfully');
    }

    /**
     *
     * @param control
     */
    public noWhitespaceValidator(control: FormControl) {
        const isWhitespace = (control.value || '').trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : {'whitespace': true};
    }
}
